# About Mac Address Validate Test

The purpose of this program is to parse Mac address of the device via the Api enpoint and return its vendor information if possible.

## Installation:

Clone the repo.
run: `composer install`

configure: `.env` file with your database credentials and correct url for project

run: `php artisan generate:key`

## Usage:
The program contains console command to update vendor data from external server.

### Console command details:

Please run first: `php artisan import:OUIData`
you will be then prompted to complete the upload.

Once data has been downloaded and database has been seeded you can now use api endpoints to lookup data.

### Api endpoint details:
Please use Get request to:
- `api/mac-lookup/find` endpoint to lookup single mac address.

Request body (json):
```json
{
    "mac_address": "00-1B-63-84-45-E6"
}
```

Please use Post request to lookup multiple mac addresses:
- `api/mac-lookup/find-many` endpoint to lookup multiple mac addresses.

Request body (json):
```json
{
    "mac_addresses":[
        "00-1B-63-84-45-E6",
        "00:1b:63:84:45:e6"
    ]
}
```

Sample responses:

The response will be returned in the following json format:

- `/find`:
```json
{
    "mac_address": "2015821A0E60",
    "vendor": "Apple Inc."
}
```
- `/find-many`:

```json
[{
    "mac_address":"00-1B-63-84-45-E6",
    "vendor":"Apple, Inc."
},{
    "mac_address":"00:1b:63:84:45:e6",
    "vendor":"Apple, Inc."
}]
```
