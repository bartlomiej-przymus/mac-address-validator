<?php

namespace App\Console\Commands;

use App\Imports\IeeeOUIDataImport;
use App\Models\IeeeOUIData;
use App\Services\OUIDownloader;
use Illuminate\Console\Command;
use Throwable;

class UpdateOUI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:OUIData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads and updates IEEE OUI data set';

    public function __construct(
        public OUIDownloader $ouiDownloader,
    ){
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Throwable
     */
    public function handle()
    {
        $this->info('Running this command will empty all database records for Ieee OUI Data.
        Updater will then attempt to download fresh copy of the OUI data from remote server.
        After successful download program will attempt to import all the records from file to database.');

        if ($this->confirm('Do you wish to continue?', true)) {

            IeeeOUIData::truncate();

            $this->info('File download started. Please stand by.');

            $path = $this->ouiDownloader->download();

            $this->info('File download finished.');

            $this->info('Processing of import data...');

            (new IeeeOUIDataImport)->withOutput($this->output)->import($path);

            $this->info('Processing of import data complete!');
        }

        return Command::SUCCESS;
    }
}
