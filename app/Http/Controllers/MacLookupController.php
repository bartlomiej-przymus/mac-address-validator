<?php

namespace App\Http\Controllers;

use App\Http\Requests\FindMacAddressesRequest;
use App\Http\Requests\FindMacAddressRequest;
use App\Models\IeeeOUIData;
use Illuminate\Support\Str;

class MacLookupController extends Controller
{
    public function find(FindMacAddressRequest $request)
    {
        $lookupData = $this->getMacAddressLookupData($request->input('mac_address'));

        $result = IeeeOUIData::query()
            ->where('assignment', $lookupData)
            ->first();

        if (is_null($result)) {
            return response()->json([
                'mac_address' => $request->input('mac_address'),
                'message' => 'Vendor data not Found!'
            ], 404);
        }

        return response()->json([
            'mac_address' => $request->input('mac_address'),
            'vendor' => $result->getVendorName(),
        ]);
    }

    public function findMany(FindMacAddressesRequest $request)
    {
        $response = [];

        foreach ($request->input('mac_addresses') as $macAddress) {
            $lookupData = $this->getMacAddressLookupData($macAddress);

            $macAddressData = IeeeOUIData::query()
                ->where('assignment', $lookupData)
                ->first();

            if (is_null($macAddressData)) {
                $result = [
                    'mac_address' => $macAddress,
                    'message' => 'Vendor data not Found!'
                ];
            } else {
                $result = [
                    'mac_address' => $macAddress,
                    'vendor' => $macAddressData->getVendorName(),
                ];
            }

            $response[] = $result;
        }

        return response()->json($response);
    }

    public function getMacAddressLookupData(string $macAddress): string
    {
        $sanitizedString = preg_replace("/[^A-Fa-f0-9]/", "", $macAddress);

        return Str::limit(Str::upper($sanitizedString), 6, '');
    }
}
