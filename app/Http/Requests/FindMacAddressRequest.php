<?php

namespace App\Http\Requests;

use App\Rules\ValidMacAddress;
use Illuminate\Foundation\Http\FormRequest;

class FindMacAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'mac_address' => [
                'required',
                new ValidMacAddress()
            ],
        ];
    }
}
