<?php

namespace App\Imports;

use App\Models\IeeeOUIData;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class IeeeOUIDataImport implements ToModel, WithHeadingRow, WithProgressBar
{
    use Importable;

    public function model(array $row)
    {
        return new IeeeOUIData([
            'registry' => $row['registry'],
            'assignment' => $row['assignment'],
            'organization_name' => $row['organization_name'],
            'organization_address' => $row['organization_address'],
        ]);
    }
}
