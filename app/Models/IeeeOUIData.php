<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IeeeOUIData extends Model
{
    use HasFactory;

    protected $fillable = [
        'registry',
        'assignment',
        'organization_name',
        'organization_address',
    ];

    public function getVendorName(): string
    {
        return $this->organization_name;
    }
}
