<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Str;

class ValidMacAddress implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $macAddress = $this->sanitizeMacAddress($value);

        if (! $this->isMacAddressCorrectLength($macAddress)) {
            $fail('Provided Mac Address has incorrect number of characters. Correct length excluding dots colons or hyphens is 12 characters.');
        }

        if ($this->isMacAddressRandom($macAddress)) {
            $fail('Mac Address is known to be random, I\'m unable to match it against database');
        }
    }

    public function sanitizeMacAddress(string $macAddress): string {
        return preg_replace("/[^A-Fa-f0-9]/", "", $macAddress);
    }

    public function isMacAddressCorrectLength(string $macAddress): bool {
        return strlen($macAddress) == 12;
    }

    public function isMacAddressRandom(string $macAddress): bool {
        return in_array($macAddress[1], ['2', '6', 'A', 'E']);
    }
}
