<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class OUIDownloader
{
    public function __construct(
        public Client $client = new Client()
    ){}

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function download(): string
    {
        $response = $this->client->get('http://standards-oui.ieee.org/oui/oui.csv');

        if ($response->getStatusCode() == 200) {
            $fileContent = $response->getBody()->getContents();

            $filePath = storage_path('ouidata.csv');

            file_put_contents($filePath, $fileContent);
        } else {
            throw new Exception("Error downloading file.");
        }

        return $filePath;
    }
}
