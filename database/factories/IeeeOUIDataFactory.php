<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class IeeeOUIDataFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'registry' => 'MA-L',
            'assignment' => Str::title(fake()->hexColor()), //oui is 6 random hex character like hex color :)
            'organization_name' => fake()->company(),
            'organization_address' => fake()->address(),
            'created_at' => now(),
        ];
    }
}
