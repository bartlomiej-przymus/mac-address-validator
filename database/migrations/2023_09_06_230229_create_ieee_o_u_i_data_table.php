<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ieee_o_u_i_data', function (Blueprint $table) {
            $table->id();
            $table->string('registry');
            $table->string('assignment')->index();
            $table->string('organization_name')->nullable();
            $table->string('organization_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ieee_o_u_i_data');
    }
};
